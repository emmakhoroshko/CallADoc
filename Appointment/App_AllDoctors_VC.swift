//
//  App_AllDoctors_VC.swift
//  Appointment
//
//  Created by Emma Khoroshko on 15.11.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

import UIKit

class App_AllDoctors_VC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet var allContentTableView: UITableView!
    var doctors = [Doctor]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.allContentTableView.delegate = self
        self.allContentTableView.dataSource = self
        
        self.allContentTableView.rowHeight = UITableViewAutomaticDimension
        self.allContentTableView.estimatedRowHeight = 148
        self.allContentTableView.register(UINib.init(nibName: "DoctorCell", bundle: nil), forCellReuseIdentifier: "DoctorCell")
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        doctors  = AllContent.shared.docArray
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return doctors.count
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell: DoctorCell = self.allContentTableView.dequeueReusableCell(withIdentifier: "DoctorCell") as! DoctorCell
        let doc = doctors[indexPath.row]
        
        cell.fillDoctorCell(type: doc.type, hospitName: doc.hospital.name, hospitAddress: doc.hospital.address, docName: doc.name)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        performSegue(withIdentifier: "AppointmentDetailViewController", sender: indexPath)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "AppointmentDetailViewController" , let vc =  segue.destination as? AppointmentDetailViewController , let indexPath =  self.allContentTableView.indexPathForSelectedRow {
            vc.doctor = self.doctors[indexPath.row]
            allContentTableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
}
