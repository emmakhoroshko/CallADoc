//
//  CustomColors.swift
//  Appointment
//
//  Created by Emma Khoroshko on 17.11.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

import Foundation
import UIKit


extension UIColor {
    @nonobjc class var acStrawberry: UIColor {
//        return UIColor(red: 0 / 255.0, green: 63.0 / 255.0, blue: 148.0 / 255.0, alpha: 1.0)
        
        //(0,53,148)
        return UIColor(red: 243.0 / 255.0, green: 61.0 / 255.0, blue: 65.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var acWhite: UIColor {
        return UIColor(white: 240.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var acBlack: UIColor {
        return UIColor(white: 0.0, alpha: 1.0)
    }
    //245,245,245
    @nonobjc class var acCharcoalGrey: UIColor {
        return UIColor(red: 56.0 / 255.0, green: 61.0 / 255.0, blue: 65.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var acLightGray: UIColor {        
        return UIColor(red: 245.0 / 255.0, green: 245.0 / 255.0, blue: 245.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var acWhiteThree: UIColor {
        return UIColor(white: 217.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var acCoolGrey: UIColor {
        //#737576 (115,117,118)
        //return UIColor(red: 142.0 / 255.0, green: 145.0 / 255.0, blue: 147.0 / 255.0, alpha: 1.0)
        return UIColor(red: 115.0 / 255.0, green: 117.0 / 255.0, blue: 118.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var acSlateGrey: UIColor {
        //#737576
        //return UIColor(red: 100.0 / 255.0, green: 104.0 / 255.0, blue: 106.0 / 255.0, alpha: 1.0)
        return UIColor(red: 115.0 / 255.0, green: 117.0 / 255.0, blue: 118.0 / 255.0, alpha: 1.0)
    }
}
