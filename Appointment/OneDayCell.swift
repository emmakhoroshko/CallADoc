//
//  OneDayCell.swift
//  Appointment
//
//  Created by Emma Khoroshko on 16.11.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

import UIKit
import JTAppleCalendar

class OneDayCell: JTAppleCell  {
    
    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var dayLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupViews()
        
        
    }
    func setupViews(){
        roundView.backgroundColor = UIColor.acStrawberry
        
    }
    
    func fillOneDayCell(state: CellState){
        
        self.dayLabel.text = state.text
        
        if state.isSelected {
            self.roundView.isHidden = false
        }else{
            self.roundView.isHidden = true
        }
        
    }
    
    func setSelecredOneDayCell(){
        
        
        self.roundView.isHidden = false
        self.roundView.backgroundColor = UIColor.acStrawberry
    }
    
    
    
    func setCellSettings(state: CellState) {
        
        
        let todayDay = HelpfulMethods.shared.format(formatStr: "YYYYMMdd", date: Date())
        let day = HelpfulMethods.shared.format(formatStr: "YYYMMdd", date: state.date)
        
        self.dayLabel.textColor = day <= todayDay ? UIColor.lightGray : UIColor.black
        self.isHidden = state.dateBelongsTo == .thisMonth ? false : true
        self.isUserInteractionEnabled = day >= todayDay ? true : false
        
        if todayDay == day {
            
            self.roundView.isHidden = false
            self.dayLabel.textColor = UIColor.black
            self.roundView.backgroundColor = UIColor.acLightGray
            
        }
    }
}
