//
//  TimeCollectionViewCell.swift
//  Appointment
//
//  Created by Emma Khoroshko on 15.11.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

import UIKit

class TimeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var timeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.sizeToFit()
        // Initialization code
    }
    
    func fillTimeCellDefault (time : String){
        timeLabel.text = time
        self.timeLabel.textColor = UIColor.acBlack
        self.isUserInteractionEnabled = true
    }
    
    func fillTimeCellDisable (time : String){
        timeLabel.text = time
        self.isUserInteractionEnabled = false
        self.timeLabel.textColor = UIColor.lightGray
    }
    
    func isSelectedTimeCell(_ selected: Bool){
        self.timeLabel.textColor =   selected ? UIColor.acStrawberry : UIColor.acBlack
        
    }
    
}
