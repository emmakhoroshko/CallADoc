//
//  Models.swift
//  Appointment
//
//  Created by Emma Khoroshko on 17.11.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

import Foundation

struct Doctor {
    
    var name = String()
    var type = String ()
    var workDateArray = [String : [Int]]()
    var hospital = Hospital()
    
}


struct Hospital {
    
    var name = String()
    var phoneNumber = String()
    var address = String()
    
}
