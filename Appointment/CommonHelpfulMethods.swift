//
//  CommonHelpfulMethods.swift
//  Appointment
//
//  Created by Emma Khoroshko on 17.11.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

import Foundation



class HelpfulMethods {
    let formatter = DateFormatter()
    static let shared = HelpfulMethods()
    
    func format(formatStr:String , date: Date)->String {
        formatter.dateFormat = formatStr
        return formatter.string(from: date)
    }
    
    

}
