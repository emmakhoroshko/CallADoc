//
//  AllContentManager.swift
//  Appointment
//
//  Created by Emma Khoroshko on 16.11.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

import Foundation




class AllContent : NSObject {
    
    static let shared = AllContent()
    
    var hos = [
        Hospital.init(name: "Боткинская больница 2", phoneNumber: "89998587654", address: "Энгельса,12"),
        Hospital.init(name: "Больница 2", phoneNumber: "89998587654", address: "Энгельса,41"),
        Hospital.init(name: "Больница 7", phoneNumber: "89998587654", address: "Энгельса,22")
    ]
    
    
    var docArray = [Doctor.init(name: "Януш Федор Константинович", type: "Эндокринолог", workDateArray: ["21.11.2017":[5,13],"22.11.2017":[5,4,13]], hospital: Hospital.init(name: "Боткинская больница 2", phoneNumber: "89998587654", address: "Энгельса,12")),
                    Doctor.init(name: "Иванова Ольга Владимировна", type: "Невролог", workDateArray:["17-11-2017":[4,5,7],"19.11.2017":[2,4,6,7,8,9]], hospital: Hospital.init(name: "Больница 2", phoneNumber: "89998587654", address: "Энгельса,14")),
                    Doctor.init(name: "Назарова Анна Петровна", type: "Терапевт", workDateArray:["19.11.2017":[0,1]], hospital: Hospital.init(name: "Боткинская больница 2", phoneNumber: "89998587654", address: "Энгельса,13"))]
    
    
    var workTime = ["8:00","8:30","9:00","9:30","10:00","10:30","11:00","11:30","12:00","12:30","13:00","14:30","15:00","15:30","16:00","16:30","17:00"]
    
    func resetDoctorInfo(doct: Doctor, time:[Int], day:String) {
        var i = 0
        for doc in docArray {
            if doc.name == doct.name {
                docArray[i] = doct
            }
            i += 1
        }
    }
}
