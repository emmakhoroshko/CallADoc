//
//  AppointmentDetailViewController.swift
//  Appointment
//
//  Created by Emma Khoroshko on 16.11.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

import UIKit
import JTAppleCalendar

class AppointmentDetailViewController: UIViewController, UICollectionViewDataSource,UICollectionViewDelegate {
    
    @IBOutlet weak var calendarV: JTAppleCalendarView!
    @IBOutlet weak var timeCollectionView: UICollectionView!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var doctorType: UILabel!
    @IBOutlet weak var timeAppointment: UILabel!
    @IBOutlet weak var dateAppointment: UILabel!
    @IBOutlet weak var doctorsName: UILabel!
    @IBOutlet weak var bookIt: UIButton!
    
    let formatter = DateFormatter()
    var selectedDataTimeArray = [String]()
    var doctor = Doctor()
    var bookedTime = [Int]()
    
    var currentDay = String()
    
    var currentbookedTime = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.calendarSetView()
        self.timeCollectionView.delegate = self
        self.timeCollectionView.dataSource = self
        self.refreshAppointmentBanner(date: "-" , time: "-")
        selectedDataTimeArray = self.createTimeArray(currentDoctor: doctor)
        
        self.timeCollectionView.register(UINib.init(nibName: "TimeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "TimeCollectionViewCell")
    }
    
    
    func calendarSetView(){
        
        calendarV.minimumLineSpacing = 0
        calendarV.minimumInteritemSpacing = 0
        //calendarV.layoutSubviews()
        calendarV.allowsDateCellStretching = false
        calendarV.visibleDates { (segmentInfo) in
            self.setupCalendarView(dateInfo: segmentInfo)
        }
        //calendarV.scrollingMode = .stopAtEachSection
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: TimeCollectionViewCell = self.timeCollectionView.dequeueReusableCell(withReuseIdentifier: "TimeCollectionViewCell", for: indexPath) as! TimeCollectionViewCell
        
        cell.fillTimeCellDefault(time: selectedDataTimeArray[indexPath.row])
        
        for i in bookedTime {
            if indexPath.row == i {
                // disable booked time
                cell.fillTimeCellDisable(time: selectedDataTimeArray[indexPath.row])
            }
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedDataTimeArray.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell: TimeCollectionViewCell = self.timeCollectionView.cellForItem(at: indexPath) as! TimeCollectionViewCell
        currentbookedTime = cell.timeLabel.text!
        self.refreshAppointmentBanner(date: currentDay , time: currentbookedTime)
        cell.isSelectedTimeCell(true)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        let cell: TimeCollectionViewCell = self.timeCollectionView.cellForItem(at: indexPath) as! TimeCollectionViewCell
        cell.isSelectedTimeCell(false)
    }
    
    
    @IBAction func makeAppointment(_ sender: UIButton) {
        self.setNewWorkTimeToDoctor()
        self.refreshTimeCollectionView()
        
    }
    
    func setNewWorkTimeToDoctor (){
        var timeArray = self.setBookedTimeArray(date: currentDay)
        if let indexCurrentTime = selectedDataTimeArray.index(of: currentbookedTime) {
            timeArray.append(indexCurrentTime)
        }
        doctor.workDateArray.updateValue(timeArray, forKey: currentDay)
        AllContent.shared.resetDoctorInfo(doct: doctor ,time: timeArray, day: currentDay)
        
    }
    
    
    func refreshTimeCollectionView (){
        self.bookedTime = self.setBookedTimeArray(date: currentDay)
        self.timeCollectionView.reloadData()
    }
}



extension AppointmentDetailViewController: JTAppleCalendarViewDelegate, JTAppleCalendarViewDataSource {
    
    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        
    }
    
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        
        formatter.dateFormat = "yyyy MM dd"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        
        let startDate = formatter.date(from: "2017 11 01")!
        let endDate = formatter.date(from: "2018 10 01")!
        let param = ConfigurationParameters(startDate: startDate, endDate: endDate, numberOfRows: 5, calendar: Calendar.current, generateInDates: .forAllMonths, generateOutDates: .tillEndOfGrid , firstDayOfWeek: .monday, hasStrictBoundaries: true)
        return param
        
    }
    
    
    
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        
        let cell: OneDayCell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "OneDayCell", for: indexPath)as! OneDayCell
        cell.fillOneDayCell(state: cellState)
        self.configurateCell(cell: cell, state: cellState)
        return cell
        
    }
    
    
    
    func setupCalendarView(dateInfo: DateSegmentInfo){
        guard let monthDate = dateInfo.monthDates.first?.date else {return}
        monthLabel.text = HelpfulMethods.shared.format(formatStr: "MMM", date: monthDate)
        yearLabel.text = HelpfulMethods.shared.format(formatStr: "yyyy", date: monthDate)
        
    }
    
    
    func configurateCell (cell: JTAppleCell, state: CellState){
        guard let currentCell = cell as? OneDayCell else {return}
        
        currentCell.setCellSettings(state: state)
        
        
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        setupCalendarView(dateInfo: visibleDates) //update calendarV when scrolling
    }
    
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        
        self.bookedTime = []
        
        guard  let currentCell = cell as? OneDayCell else {return}
        
        currentCell.setSelecredOneDayCell()
        
        currentDay = HelpfulMethods.shared.format(formatStr: "dd.MM.yyyy", date: date)
        self.refreshAppointmentBanner(date: currentDay , time: "-")
        
        self.refreshTimeCollectionView()
    }
    
    
    func setBookedTimeArray (date: String) -> [Int]{
        
        //let cellDate = self.format(formatStr: "dd.MM.yyyy", date: date)
        if let tmp = doctor.workDateArray[date] {
            
            return tmp  // create boooked time array
        }else{
            return []
        }
    }
    
    
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        guard  let currentCell = cell as? OneDayCell else {return}
        currentCell.roundView.isHidden = true
        
    }
    
    func createTimeArray(currentDoctor: Doctor) -> [String]{
        let workTime =  AllContent.shared.workTime // daily time for all doctors
        let cellDate = HelpfulMethods.shared.format(formatStr: "dd.MM.yyyy", date: Date())
        self.bookedTime = self.setBookedTimeArray(date: cellDate) //create bookedarray for today
        return workTime
    }
    
    
    
    
    func refreshAppointmentBanner (date: String , time: String){
        
        doctorsName.text = doctor.name
        doctorType.text = doctor.type
        timeAppointment.text = time
        dateAppointment.text = date
    }
    
    
    
    
}
