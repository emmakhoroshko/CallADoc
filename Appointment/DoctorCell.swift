//
//  DoctorCell.swift
//  Appointment
//
//  Created by Emma Khoroshko on 15.11.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

import UIKit

class DoctorCell: UITableViewCell {
    
    @IBOutlet weak var doctorType: UILabel!
    @IBOutlet weak var doctorTypeImg: UIImageView!
    @IBOutlet weak var hospitalName: UILabel!
    @IBOutlet weak var hospitalAddress: UILabel!
    @IBOutlet weak var doctorName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func fillDoctorCell (type: String, hospitName:String, hospitAddress: String, docName: String){
        
        doctorType.text = type
        doctorName.text = docName
        hospitalName.text = hospitName
        hospitalAddress.text = hospitAddress
        
        
    }
    
    
}
