//
//  CustomUIElementsClasses.swift
//  Appointment
//
//  Created by Emma Khoroshko on 17.11.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

import UIKit

class CustomUIElementsClasses: NSObject {

}

class CustomUiView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
    }
    
    func commonInit(){
        self.layer.cornerRadius = 4
        self.backgroundColor = UIColor.acLightGray
        self.tintColor = UIColor.acWhite
    }
    
    
    
}
class CustomLabelTitle: UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
    }
    
    func commonInit(){
        self.textColor = UIColor.acStrawberry
        self.font = UIFont.boldSystemFont(ofSize: 20)
    }
}

class CustomLabelH2: UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
    }
    
    func commonInit(){
        self.textColor = UIColor.acCharcoalGrey
        self.font = UIFont.boldSystemFont(ofSize: 17)
    }
}


class CustomButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
    }
    
    func commonInit(){
        self.layer.cornerRadius = 4
        self.backgroundColor = UIColor.acStrawberry
        self.titleLabel?.textColor =  UIColor.acWhite
        self.tintColor = UIColor.acWhite
    }
    
    
    
}
