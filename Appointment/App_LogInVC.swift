//
//  App_LogInVC.swift
//  Appointment
//
//  Created by Emma Khoroshko on 15.11.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

import UIKit


class App_LogInVC: UIViewController {
    
    @IBOutlet weak var mailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var logInButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.createNavigationBar()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        self.view.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        mailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }
    
    @IBAction func AcceptPasswordButton(_ sender: UIButton) {
        self.checkCorrectData()
        
    }
    
    @IBAction func forceLogin(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "AllDoctors", sender: nil)
    }
    
    func checkCorrectData(){
        if  let mail =  mailTextField.text, let pass = passwordTextField.text {
            print(mail)
            
            LogInManager.shared.loginAccept(name: mail, password: pass) { (correct) in
                if correct {
                    self.performSegue(withIdentifier: "AllDoctors", sender: nil)
                } else {
                    print("Отказано в доступе")
                }
            }
        }
    }
    
    func createNavigationBar (){
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Выход", style: .plain, target: nil, action: nil)
        
        self.navigationController?.navigationBar.tintColor = UIColor.acStrawberry
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
}
